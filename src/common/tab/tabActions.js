export const  selectTab = (id) => {
    
    return dispatch => {
       dispatch({
        type:'TAB_SELECTED',
        payload:id
       })
    }
}

export const showTabs = (...tabs) => {
    const tabsToShow  = {}
    tabs.forEach(e => tabsToShow[e] = true)
    return dispatch => {
       dispatch({
        type:'TYPE_SHOWED',
        payload:tabsToShow
       })
    }
}