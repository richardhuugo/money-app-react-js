import React from 'react'
import {selectTab} from './tabActions'
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import If from '../operador/if'
 class TabHeader extends React.Component{
    render(){
        const selected = this.props.tab.selected === this.props.target  
        const visible = this.props.tab.visible[this.props.target]      
        return(
        <If test={visible}>
            <li
                className={selected ? 'active' : ''}
            >
                <a href="javascrip:;"
                data-toggle="tab"
                onClick={() => {this.props.selectTab(this.props.target)}}
                data-target={this.props.target}                
                >
                <i className={`fa fa-${this.props.icon}`}></i>{this.props.label}
                </a>
            </li>
        </If>
        )
    }
}

const mapStateToProps = state => ({
  
    tab: state.tabReducer
})
export default connect(mapStateToProps,{selectTab})(TabHeader)