import React from 'react'
import Grid from '../common/layout/grid'
import Row from '../common/layout/row'
import ValueBox from '../common/widget/valueBox'

export default ({credits,debts}) => (
    <Grid cols="12">
        <fieldset>
            <legend>Resumo</legend>
            <Row>
                <ValueBox  cols="12 4" color="green" icon="bank" text="Total de créditos" value={`R$ ${credits}`} />
                <ValueBox  cols="12 4" color="red" icon="credit-card" text="Total de débitos" value={`R$ ${debts}`} />
                <ValueBox  cols="12 4" color="blue" icon="money" text="Valor consolidado" value={`R$ ${credits - debts}`} />
            </Row>
        </fieldset>
    </Grid>
)