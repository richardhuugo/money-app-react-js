import React from 'react'
import Grid from '../common/layout/grid'
import {Field, arrayInsert,arrayRemove} from 'redux-form'
import Input from '../common/form/input'
import If from '../common/operador/if'
import {connect} from 'react-redux'


 class CreditList extends React.Component{
    add(index,item = {}){
        if(!this.props.readOnly){
            this.props.arrayInsert('CicloForm', 'credits',index,item)
        }
    }
    remove(index){
        if(!this.props.readOnly && this.props.list.length >1){
            this.props.arrayRemove('CicloForm', 'credits',index)
        }
    }
    rendeRows(){
        const  list = this.props.list || []
        return list.map((item,index) => (
            <tr key={index}>
            <td>
                <Field component={Input} name={`${this.props.field}[${index}].name`} 
                placeholder="Informe o nome"
                readOnly={this.props.readOnly}
                />
            </td>
            <td>
                <Field  component={Input} name={`${this.props.field}[${index}].value`}  
                 placeholder="Informe o valor"
                 readOnly={this.props.readOnly}/>
            </td>
            <If test={this.props.showStatus} >
            <td>
                <Field  component={Input} name={`${this.props.field}[${index}].status`}  
                 placeholder="Informe o status"
                 readOnly={this.props.readOnly}/>
            </td>
            </If>
            <td>
                <button type="button" className="btn btn-success"
                onClick={() =>this.add(index +1)}
                >
                    <i className="fa fa-plus"></i>
                </button>
                <button type="button" className="btn btn-warning"
                onClick={() =>this.add(index +1,item)}
                >
                    <i className="fa fa-clone"></i>
                </button>
                <button type="button" className="btn btn-danger"
                onClick={() =>this.remove(index)}
                >
                    <i className="fa fa-trash"></i>
                </button>
            </td>
        </tr>
        ))
       
    }

    render(){
        return(
            <Grid
            cols={this.props.cols}
            >
                <fieldset >
                    <legend>{this.props.legend}</legend>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>
                                    Nome
                                </th>
                                <th>Valor</th>
                                <If test={this.props.showStatus} >
                                <th>Status</th>
                                </If>
                                <th>Ações</th>
                               
                            </tr>
                        </thead>
                        <tbody>
                            {this.rendeRows()}
                        </tbody>
                    
                    </table>
                </fieldset>

            </Grid>
        )
    }
}
export default connect(null,{arrayInsert,arrayRemove})(CreditList)