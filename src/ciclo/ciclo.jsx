import React from 'react'
import {connect} from 'react-redux'

import {init, create,update,remove} from './cicloActions'
import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import Tabs from '../common/tab/tabs'
import Tabscontent from '../common/tab/tabsContent'
import TabsHeader from '../common/tab/tabsHeader'
import TabHeader from '../common/tab/tabHeader'
import TabContent from '../common/tab/tabContent'

import CicloList from './cicloList'
import Form from './cicloForm'

class Ciclo extends React.Component{

   componentDidMount(){
       this.props.init()
   }

    render() {
        return (
            <div>
                <ContentHeader title="Ciclo de pagamentos" small="Cadastro" />
                <Content>
                  <Tabs>
                      <TabsHeader>
                          <TabHeader
                          icon="bars"
                          target="tablist"
                          label="Listar"
                          />

                          <TabHeader
                          icon="plus"
                          target="tabcreate"
                          label="Incluir"
                          />

                          <TabHeader
                          icon="pencil"
                          target="tabupdate"
                          label="Alterar"
                          />

                          <TabHeader
                          icon="trash"
                          target="tabdelete"
                          label="Exluir"
                          />
                      </TabsHeader>
                      <Tabscontent>
                          <TabContent id="tablist">
                            <CicloList />
                          </TabContent>

                        <TabContent id="tabcreate">
                             <Form onSubmit={this.props.create}
                             submitLabel="Incluir" submitClass="primary"
                             />
                          </TabContent>

                           <TabContent id="tabupdate">
                            <Form onSubmit={this.props.update}
                            
                            submitLabel="Alterar" submitClass="info"
                            />
                          </TabContent>

                           <TabContent id="tabdelete">
                           <Form onSubmit={this.props.remove} readOnly={true} 
                           submitLabel="Deletar" submitClass="danger"
                           />
                          </TabContent>
                      </Tabscontent>
                  </Tabs>
                </Content>
            </div>
        )
    }
}

export default connect(null,{init,create, update,remove})(Ciclo)