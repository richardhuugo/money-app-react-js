import React from 'react'
import {connect} from 'react-redux'
import {getList, showUpdate,showDelete } from './cicloActions'
 class CicloList extends React.Component{

    componentDidMount(){
        this.props.getList()
        
    }
    renderRows(){
        const lista = this.props.list || []
        return lista.map(bc =>(
            <tr
            key={bc._id}
            >
                <td>{bc.name}</td>
                <td>{bc.month}</td>
                <td>{bc.year}</td>
                <td>
                    <button className="btn btn-warning"
                    onClick={() => {this.props.showUpdate(bc)}}
                    >
                    <i className="fa fa-pencil">
                    </i>
                    </button>
                    <button className="btn btn-danger"
                    onClick={() => {this.props.showDelete(bc)}}
                    >
                    <i className="fa fa-trash-o">
                    </i>
                    </button>
                </td>
               
            </tr>
        ))
    }
    render(){
       
        return(
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Mes</th>
                            <th>Ano</th>
                            <th>Acões</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }
}
const mapStateToProps = state => ({  list: state.cicloReducer.list })

export default connect(mapStateToProps,{getList,showUpdate,showDelete})(CicloList)