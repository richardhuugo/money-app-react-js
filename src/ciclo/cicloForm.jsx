import React from 'react'
import {reduxForm, Field, formValueSelector} from 'redux-form'
import LabelInput from '../common/form/labelInput'
import CreditList from '../ciclo/creditList'
import {connect} from 'react-redux'
import {init} from './cicloActions'
import Summary from './summary'
class CicloForm extends React.Component{

    calcularSummary(){
        const sum = (t, v) => t + v
        return {
            somaCredits: this.props.credits.map(c => +c.value || 0).reduce(sum),
            somaDebts: this.props.debts.map(d => +d.value || 0).reduce(sum)
            
        }
    }
    render(){
        const {handleSubmit, readOnly, credits,debts } = this.props
        const { somaCredits,somaDebts  } = this.calcularSummary()
        return(
            <form role="form" onSubmit={handleSubmit}  >
                <div className="box-body">
                    <Field name="name" component={LabelInput}  
                    readOnly={readOnly}
                        label="Nome" cols="12 4"                         
                        placeholder="Informe o nome"
                        type="text"
                    />
                    <Field name="month" component={LabelInput} 
                    readOnly={readOnly}
                    type="number"                    
                    label="Mês"
                    cols="12 4"
                    placeholder="Informe o mes"
                    
                    />
                    <Field name="year" component={LabelInput} 
                    readOnly={readOnly}
                    type="number"                    
                    label="Ano"
                    cols="12 4"
                    placeholder="Informe o ano"/>
                    <Summary credits={somaCredits} debts={somaDebts} />
                    <CreditList cols="12 6" list={credits} field="credits"  legend="Créditos"  readOnly={readOnly} />
                    <CreditList cols="12 6" list={debts} showStatus={true} field="debts" legend="Débitos"  readOnly={readOnly} />
                </div>
                <div className="box-footer">
                    <button type="submit" className={`btn btn-${this.props.submitClass}`}>
                    {this.props.submitLabel}
                    </button>
                    <button className="btn btn-default"
                    type="button"
                    onClick={this.props.init}
                    >
                    Cancelar
                    </button>
                </div>
            </form>
        )
    }
}
const cicloForm = reduxForm({form:'CicloForm', destroyOnUnmount:false })(CicloForm)
const selector = formValueSelector('CicloForm')
const mapStateToProps = state => ({
    credits:selector(state,'credits'),
    debts:selector(state,'debts')
})

export default connect(mapStateToProps,{init})(cicloForm)