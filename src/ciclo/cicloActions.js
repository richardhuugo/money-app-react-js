import axios from 'axios'
import {toastr} from 'react-redux-toastr'
const URL = "http://localhost:3003/api"
import {reset as resetForm , initialize} from 'redux-form'
import {showTabs, selectTab} from '../common/tab/tabActions'

const INITIAL_VALUES = {credits:[{}], debts:[{}]}

export const getList = () => {
    const request = axios.get(`${URL}/billingCycles`)
    return dispatch => {        
        dispatch({
            type:'CICLO_FETCHED',
            payload: request
        })
    }
}

export const create = (values) => {
    return submit(values, 'post')
}

export const update = (values) => {
   return submit(values, 'put')
}

export const remove = (values) => {
    return submit(values, 'delete')
}

function submit(values, method) {
    return dispatch => {
        const id = values._id ? values._id : ''
        
        axios[method](`${URL}/billingCycles/${id}`,values).then(resp => {
            toastr.success('Sucesso','Operação realizada com sucesso.')
            dispatch(init())
        }).catch(error => {
            
            error.response.data.errors.forEach(element => {
                toastr.error('Error','Ocorreu um error ao adicionar '+element)    
            });
            
        })
      
    }
}

// REFATORAR ESSA FUNCAO
export const showUpdate = (ciclo) => {
    return dispatch => {
        dispatch([
            showTabs('tabupdate'),
            selectTab('tabupdate'),
            initialize('CicloForm',ciclo)
        ])
    }
}

export const showDelete = (ciclo) => {
    return dispatch => {
        dispatch([
            showTabs('tabdelete'),
            selectTab('tabdelete'),
            initialize('CicloForm',ciclo)
        ])
    }
}


export const init = () => {
    return dispatch => {
        dispatch([
            showTabs('tablist','tabcreate'),
            selectTab('tablist'),
            getList(),
            initialize('CicloForm',INITIAL_VALUES)

        ])
    }
}