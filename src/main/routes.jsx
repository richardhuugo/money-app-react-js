import React from 'react'
import { Router, Route,IndexRoute,  Redirect , hashHistory} from 'react-router'

import App from './app'
import Dashboard from '../dashboard/dashboard'
import Ciclo from '../ciclo/ciclo'

export default props => (
    <Router history={hashHistory}>
        <Route path="/" component={App} >
            <IndexRoute component={Dashboard}/>                            
            <Route path="ciclo" component={Ciclo}  />
        </Route>
        

        <Redirect from="*" to="/" />
    </Router>
)
