import {combineReducers} from 'redux'
import dashboardReducer from '../dashboard/dashboardReducer'
import tabReducer from '../common/tab/tabReducer'
import cicloReducer from '../ciclo/cicloReducer'
import {reducer as formReducer } from 'redux-form'
import {reducer as toastrReducer } from 'react-redux-toastr'

const reducer = combineReducers({
    dashboard:dashboardReducer,
    tabReducer,
    cicloReducer,
    form:formReducer,
    toastr:toastrReducer
})

export default   reducer