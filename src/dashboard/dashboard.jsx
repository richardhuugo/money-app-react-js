import React from 'react'
import ContentHeader from '../common/template/contentHeader'
import Content from '../common/template/content'
import ValueBox from '../common/widget/valueBox'
import Row from '../common/layout/row'
import {connect} from 'react-redux'

import {getSummary}   from './dashboardActions'
class Dashboard extends React.Component {
      
    constructor(props){
        super(props)

        this.props.getSummary()
    }
    render(){
        const {credit, debt} = this.props.sumary    
        return(
            <div>
               
            <ContentHeader title="Dashboard" small="Versão 1.0" />
            <Content> 
            <Row>
                <ValueBox cols="12 4" color="green" icon="bank" 
                    value={`R$ ${credit}`} text="Total de créditos"
                />

                <ValueBox cols="12 4" color="red" icon="credit-card" 
                    value={`R$ ${debt}`} text="Total de debitos"
                />
                <ValueBox cols="12 4" color="blue" icon="money" 
                    value={`R$ ${credit - debt}`} text="Valor consolidado"
                />
                </Row>
                </Content>
         </div>
        )
    }
    
}
const mapStateToProps = state => ({
 sumary: state.dashboard.summary
})

export default connect(mapStateToProps,{getSummary})(Dashboard)
